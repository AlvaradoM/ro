const count = [0,0,0,0,0,0,0,0,0,0,0]

function rollDice(){
    var d1 = Math.floor(Math.random() * 6) + 1;
    var d2 = Math.floor(Math.random() * 6) + 1;
    return d1 + d2 
}
function roll(numRolls){
    for(let i = 0; i <=numRolls; i++){
        const dice_roll = rollDice()
        count[dice_roll-2] ++
    }
    return count
}
console.log(roll(1000))

function bars() {
    let dest_graph = document.getElementById("total")

    for(let i=0;i<count.length; i++){
        let bar = document.createElement("div")
        bar.style.height = "25px"
        bar.style.width = count[i]+"px"
        bar.style.backgroundColor = "limegreen"
        bar.style.margin = "20px"
        bar.style.display = "flex"
       bar.style.justifyContent = "center"
  bar.style.fontFamily = "Impact"
  bar.style.fontSize = "larger"
        let sum_text = document.createTextNode(i+2 + ':' + count[i]+ ' ')
        bar.appendChild(sum_text)
        dest_graph.appendChild(bar)
    }
}
roll (1000)
bars()
